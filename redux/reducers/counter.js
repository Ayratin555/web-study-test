import { INCREMENT_COUNTER, DECREMENT_COUNTER } from '../actions/counter';

export default function counter(state = 'live', action) {
  switch (action.type) {
  case INCREMENT_COUNTER:
    return 'die';
  case DECREMENT_COUNTER:
    return 'live';
  default:
    return state;
  }
}
