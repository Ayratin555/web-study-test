import React, { Component, PropTypes } from 'react';

class Counter extends Component {
  render() {
    const { increment, counter } = this.props;
    return (
      <p>
        {counter} 
        {' '}
        <button onClick={increment}>Click me</button>
       
      </p>
    );
  }
}

Counter.propTypes = {
  increment: PropTypes.func.isRequired,
  counter: PropTypes.string.isRequired
};

export default Counter;
