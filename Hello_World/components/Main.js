var React = require('react')

var AuthorActions = require('../actions/authorActions');
var AuthorStore = require('../stores/authorStore');

var Main = React.createClass({
    onButtonClick: function() {
    // codez to make the moneys
    this.setState({
      word: "Goodbye World"
    });
    },
     getInitialState: function(){
        return {
            word: AuthorStore.getAllAuthors()
        }
    },
    render: function(){
        return (
            <div>
              <h1>{this.state.word}</h1>
              <button onClick={this.onButtonClick}>Click Me</button>
            </div>
        )
    }
})

React.render(<Main />, document.getElementById('app')) 